/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uhn.fhir.jpa.provider.dstu3;

import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.dao.IFhirSystemDao;
import ca.uhn.fhir.jpa.entity.ForcedId;
import ca.uhn.fhir.jpa.entity.ResourceHistoryTable;
import ca.uhn.fhir.jpa.entity.ResourceHistoryTag;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamCoords;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamDate;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamNumber;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamQuantity;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamString;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamToken;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamUri;
import ca.uhn.fhir.jpa.entity.ResourceLink;
import ca.uhn.fhir.jpa.entity.ResourceTable;
import ca.uhn.fhir.jpa.entity.ResourceTag;
import ca.uhn.fhir.jpa.entity.SearchInclude;
import ca.uhn.fhir.jpa.entity.SearchResult;
import ca.uhn.fhir.jpa.entity.SubscriptionFlaggedResource;
import ca.uhn.fhir.jpa.entity.SubscriptionTable;
import ca.uhn.fhir.jpa.entity.TagDefinition;
import ca.uhn.fhir.jpa.entity.TermCodeSystem;
import ca.uhn.fhir.jpa.entity.TermCodeSystemVersion;
import ca.uhn.fhir.jpa.entity.TermConcept;
import ca.uhn.fhir.jpa.entity.TermConceptParentChildLink;
import ca.uhn.fhir.jpa.provider.BaseJpaProvider;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.param.StringParam;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.search.jpa.Search;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.CapabilityStatement;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Meta;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author esteban
 */
public class ResourcesCleanupProvider extends BaseJpaProvider {

    public final static String RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME = "resources-cleanup";
    public final static String RESOURCES_CELANUP_OPERATION_NAME = "$resources-cleanup";

    @Autowired()
    private EntityManager entityManager;

    @Autowired()
    private JpaTransactionManager jpaTransactionManager;

    @Autowired()
    @Qualifier("mySystemDaoDstu3")
    private IFhirSystemDao<Bundle, Meta> systemDao;

    private IFhirResourceDao<CapabilityStatement> capabilityDao;

    /**
     * Create a CapabilityStatement for the {@link #RESOURCES_CELANUP_OPERATION_NAME}
     * operation if it doesn't exist.
     * 
     */
    @PostConstruct
    public void postConstruct() {
        capabilityDao = systemDao.getDao(CapabilityStatement.class);
        this.createCapabilityStatement();
    }

    /**
     * Delete <strong>ALL</strong> Resources (except CapabilityStatement) from
     * the underlying store.
       *
     * @param theServletRequest
     * @return 
     */
    @Operation(name = RESOURCES_CELANUP_OPERATION_NAME, idempotent = true, returnParameters = {
        @OperationParam(name = "Status", type = StringType.class)
    })
    public Parameters cleanup(HttpServletRequest theServletRequest) {
        try {
            startRequest(theServletRequest);
            this.purgeDatabase(entityManager, jpaTransactionManager);

            Parameters retVal = new Parameters();
            retVal.addParameter().setName("Status").setValue(new StringType("success"));

            return retVal;
        } finally {
            endRequest(theServletRequest);
        }
    }

    /**
     * Create a CapabilityStatement for the {@link #RESOURCES_CELANUP_OPERATION_NAME}
     * operation if it doesn't exist.
     * 
     */
    private void createCapabilityStatement() {
        int capabilities = capabilityDao.search("name", new StringParam(RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME))
            .size();

        if (capabilities == 0) {
            CapabilityStatement cs = new CapabilityStatement()
                .setName(RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME)
                .setStatus(Enumerations.PublicationStatus.ACTIVE)
                .setExperimental(true);

            capabilityDao.create(cs);
        }
    }

    /**
     * From https://groups.google.com/forum/#!topic/hapi-fhir/5GytXSiaSow
     *
     * @param entityManager
     * @param theTxManager
     */
    private void purgeDatabase(final EntityManager entityManager, PlatformTransactionManager theTxManager) {
        TransactionTemplate txTemplate = new TransactionTemplate(theTxManager);
        txTemplate.setPropagationBehavior(TransactionTemplate.PROPAGATION_REQUIRED);
        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("UPDATE " + ResourceHistoryTable.class.getSimpleName() + " d SET d.myForcedId = null WHERE d.myResourceType != :capabilityStatement").setParameter("capabilityStatement", CapabilityStatement.class.getSimpleName()).executeUpdate();
                entityManager.createQuery("UPDATE " + ResourceTable.class.getSimpleName() + " d SET d.myForcedId = null WHERE d.myResourceType != :capabilityStatement").setParameter("capabilityStatement", CapabilityStatement.class.getSimpleName()).executeUpdate();
                return null;
            }
        });
        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE from " + SubscriptionFlaggedResource.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ForcedId.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamDate.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamNumber.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamQuantity.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamString.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamToken.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamUri.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceIndexedSearchParamCoords.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceLink.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + SearchResult.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + SearchInclude.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + TermConceptParentChildLink.class.getSimpleName() + " d").executeUpdate();
                return null;
            }
        });
        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE from " + TermConcept.class.getSimpleName() + " d").executeUpdate();
                for (TermCodeSystem next : entityManager.createQuery("SELECT c FROM " + TermCodeSystem.class.getName() + " c", TermCodeSystem.class).getResultList()) {
                    next.setCurrentVersion(null);
                    entityManager.merge(next);
                }
                return null;
            }
        });
        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE from " + TermCodeSystemVersion.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + TermCodeSystem.class.getSimpleName() + " d").executeUpdate();
                return null;
            }
        });
        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE from " + SubscriptionTable.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceHistoryTag.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceTag.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + TagDefinition.class.getSimpleName() + " d").executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceHistoryTable.class.getSimpleName() + " d WHERE d.myResourceType != :capabilityStatement").setParameter("capabilityStatement", CapabilityStatement.class.getSimpleName()).executeUpdate();
                entityManager.createQuery("DELETE from " + ResourceTable.class.getSimpleName() + " d WHERE d.myResourceType != :capabilityStatement").setParameter("capabilityStatement", CapabilityStatement.class.getSimpleName()).executeUpdate();
                entityManager.createQuery("DELETE from " + Search.class.getSimpleName() + " d").executeUpdate();
                return null;
            }
        });
    }

}
