/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.uhn.fhir.jpa.provider.dstu3;

import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.dao.IFhirSystemDao;
import ca.uhn.fhir.jpa.entity.ForcedId;
import ca.uhn.fhir.jpa.entity.ResourceHistoryTable;
import ca.uhn.fhir.jpa.entity.ResourceHistoryTag;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamDate;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamNumber;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamQuantity;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamString;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamToken;
import ca.uhn.fhir.jpa.entity.ResourceIndexedSearchParamUri;
import ca.uhn.fhir.jpa.entity.ResourceLink;
import ca.uhn.fhir.jpa.entity.ResourceTable;
import ca.uhn.fhir.jpa.entity.ResourceTag;
import ca.uhn.fhir.jpa.entity.SearchResult;
import ca.uhn.fhir.jpa.entity.SubscriptionFlaggedResource;
import ca.uhn.fhir.jpa.entity.SubscriptionTable;
import ca.uhn.fhir.jpa.entity.TagDefinition;
import ca.uhn.fhir.jpa.provider.BaseJpaProvider;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.param.StringParam;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.CapabilityStatement;
import org.hl7.fhir.dstu3.model.Enumerations;
import org.hl7.fhir.dstu3.model.Meta;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author esteban
 */
public class ResourcesCleanupByTagProvider extends BaseJpaProvider {

    public final static String RESOURCES_CELANUP_BY_TAG_OPERATION_CAPABILITY_NAME = "resources-cleanup-by-tag";
    public final static String RESOURCES_CELANUP_BY_TAG_OPERATION_NAME = "$resources-cleanup-by-tag";

    public final static String RESOURCES_CELANUP_BY_TAG_PARAM_TAG_SYSTEM = "tagSystem";
    public final static String RESOURCES_CELANUP_BY_TAG_PARAM_TAG_CODE = "tagCode";

    @Autowired()
    private EntityManager entityManager;

    @Autowired()
    private JpaTransactionManager jpaTransactionManager;

    @Autowired()
    @Qualifier("mySystemDaoDstu3")
    private IFhirSystemDao<Bundle, Meta> systemDao;

    private IFhirResourceDao<CapabilityStatement> capabilityDao;

    /**
     * Create a CapabilityStatement for the
     * {@link #RESOURCES_CELANUP_BY_TAG_OPERATION_NAME}     * operation if it doesn't exist.
     * 
     */
    @PostConstruct
    public void postConstruct() {
        capabilityDao = systemDao.getDao(CapabilityStatement.class);
        this.createCapabilityStatement();
    }

    /**
     * Delete <strong>ALL</strong> Resources from the underlying store that are
     * tagged with a specific tag.
     */
    @Operation(name = RESOURCES_CELANUP_BY_TAG_OPERATION_NAME, idempotent = false, returnParameters = {
        @OperationParam(name = "Status", type = StringType.class),
        @OperationParam(name = "Message", type = StringType.class)
    })
    //public Parameters cleanup(HttpServletRequest theServletRequest) {
    public Parameters cleanup(HttpServletRequest theServletRequest,
            @OperationParam(name = RESOURCES_CELANUP_BY_TAG_PARAM_TAG_SYSTEM) StringParam tagSystem,
            @OperationParam(name = RESOURCES_CELANUP_BY_TAG_PARAM_TAG_CODE) StringParam tagCode) {

        if (tagSystem == null || StringUtils.isBlank(tagSystem.getValue())) {
            return createReturn("fail", RESOURCES_CELANUP_BY_TAG_PARAM_TAG_SYSTEM + " parameter must be provided");
        }
        if (tagCode == null || StringUtils.isBlank(tagCode.getValue())) {
            return createReturn("fail", RESOURCES_CELANUP_BY_TAG_PARAM_TAG_CODE + " parameter must be provided");
        }

        try {
            startRequest(theServletRequest);
            this.purgeByTag(entityManager, jpaTransactionManager, tagSystem.getValue(), tagCode.getValue());

            Parameters retVal = new Parameters();
            retVal.addParameter().setName("Status").setValue(new StringType("success"));

            return retVal;
        } finally {
            this.createCapabilityStatement();
            endRequest(theServletRequest);
        }
    }

    private Parameters createReturn(String status, String message) {
        Parameters retVal = new Parameters();
        retVal.addParameter().setName("Status").setValue(new StringType(status));
        retVal.addParameter().setName("Message").setValue(new StringType(message));
        return retVal;
    }

    /**
     * Create a CapabilityStatement for the
     * {@link #RESOURCES_CELANUP_BY_TAG_OPERATION_NAME}     * operation if it doesn't exist.
     * 
     */
    private void createCapabilityStatement() {
        int capabilities = capabilityDao.search("name", new StringParam(RESOURCES_CELANUP_BY_TAG_OPERATION_CAPABILITY_NAME))
                .size();

        if (capabilities == 0) {
            CapabilityStatement cs = new CapabilityStatement()
                    .setName(RESOURCES_CELANUP_BY_TAG_OPERATION_CAPABILITY_NAME)
                    .setStatus(Enumerations.PublicationStatus.ACTIVE)
                    .setExperimental(true);

            capabilityDao.create(cs);
        }
    }

    /**
     * Based on https://groups.google.com/forum/#!topic/hapi-fhir/5GytXSiaSow
     *
     * @param entityManager
     * @param theTxManager
     */
    private void purgeByTag(final EntityManager entityManager, PlatformTransactionManager theTxManager, final String tagSystem, final String tagCode) {
        TransactionTemplate txTemplate = new TransactionTemplate(theTxManager);
        txTemplate.setPropagationBehavior(TransactionTemplate.PROPAGATION_REQUIRED);

        final List resourcesIds = txTemplate.execute(new TransactionCallback<List<Long>>() {
            @Override
            public List<Long> doInTransaction(TransactionStatus theStatus) {
                return entityManager.createNativeQuery(""
                        + "SELECT R.RES_ID FROM HFJ_RESOURCE R, HFJ_RES_TAG RT, HFJ_TAG_DEF TD "
                        + "WHERE "
                        + "TD.TAG_SYSTEM = ? AND "
                        + "TD.TAG_CODE = ? AND "
                        + "RT.TAG_ID = TD.TAG_ID AND "
                        + "R.RES_ID = RT.RES_ID")
                        .setParameter(1, tagSystem)
                        .setParameter(2, tagCode)
                        .getResultList();
            }
        });

        if (resourcesIds == null || resourcesIds.isEmpty()) {
            return;
        }

        //Even if we are supposed to have a List<Long>, hibernate returns a List<BigInteger>.
        //We need to make sure we have Longs
        final List<Long> resourcesIdsAsLongs = new ArrayList<Long>();
        for (Object resourcesId : resourcesIds) {
            resourcesIdsAsLongs.add(((Number) resourcesId).longValue());
        }

        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("UPDATE " + ResourceHistoryTable.class.getSimpleName() + " d SET d.myForcedId = null WHERE d.myResourceId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("UPDATE " + ResourceTable.class.getSimpleName() + " d SET d.myForcedId = null WHERE d.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                return null;
            }
        });

        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE FROM " + SubscriptionFlaggedResource.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ForcedId.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamDate.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamNumber.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamQuantity.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamString.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamToken.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceIndexedSearchParamUri.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceLink.class.getSimpleName() + " d WHERE d.mySourceResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + SearchResult.class.getSimpleName() + " d WHERE d.myResourcePid IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                //entityManager.createQuery("DELETE FROM " + SearchInclude.class.getSimpleName() + " d WHERE d.myResourcePid.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                //entityManager.createQuery("DELETE FROM " + TermConceptParentChildLink.class.getSimpleName() + " d WHERE d.myResourcePid.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                return null;
            }
        });

        txTemplate.execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus theStatus) {
                entityManager.createQuery("DELETE FROM " + SubscriptionTable.class.getSimpleName() + " d WHERE d.myResId.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceHistoryTag.class.getSimpleName() + " d WHERE d.myResourceId.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceTag.class.getSimpleName() + " d WHERE d.myResource.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + TagDefinition.class.getSimpleName() + " d WHERE d.mySystem = :system AND d.myCode = :code").setParameter("system", tagSystem).setParameter("code", tagCode).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceHistoryTable.class.getSimpleName() + " d WHERE d.myResourceId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                entityManager.createQuery("DELETE FROM " + ResourceTable.class.getSimpleName() + " d WHERE d.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                //entityManager.createQuery("DELETE FROM " + Search.class.getSimpleName() + " d WHERE d.myId IN :resIds").setParameter("resIds", resourcesIdsAsLongs).executeUpdate();
                return null;
            }
        });

    }

}
