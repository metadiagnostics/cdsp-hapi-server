hapi-fhir
=========

HAPI FHIR - Java API for HL7 FHIR Clients and Servers

[![Build Status](https://travis-ci.org/jamesagnew/hapi-fhir.svg?branch=master)](https://travis-ci.org/jamesagnew/hapi-fhir)
[![Coverage Status](https://coveralls.io/repos/jamesagnew/hapi-fhir/badge.svg?branch=master&service=github)](https://coveralls.io/github/jamesagnew/hapi-fhir?branch=master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/ca.uhn.hapi.fhir/hapi-fhir-base/badge.svg)](http://search.maven.org/#search|ga|1|ca.uhn.hapi.fhir)
[![Dependency Status](https://www.versioneye.com/user/projects/55e1d0d9c6d8f2001c00043e/badge.svg?style=flat)](https://www.versioneye.com/user/projects/55e1d0d9c6d8f2001c00043e)
[![License](https://img.shields.io/badge/license-apache%202.0-60C060.svg)](http://jamesagnew.github.io/hapi-fhir/license.html)

Complete project documentation is available here:
http://jamesagnew.github.io/hapi-fhir/

A demonstration of this project is available here:
http://fhirtest.uhn.ca/

This project is Open Source, licensed under the Apache Software License 2.0.

This enhanced FHIR version has the following modifications which have been proposed to be added to HAPI-FHIR 2.5:
* Adds rest-hook subscriptions
* Modifies the existing websocket subscriptions to be event driven rather than the original code which polls every 10 seconds
* Adds notification of deletes (current FHIR specification only notifies on create and update, and does not include a verb of whether it was a create or update operation)
* Adds the ability to schedule Tminus subscription functionality when using the effective date. (e.g. Notify of observation changes in the last 1 hour)
* Adds the ability to return a bundle of data in a subscription rest-hook callback.  You can now get an empty notification, the item that was changed or have a customized query to return a bundle of data)  

# Binary
Copy 
hapi-fhir/deploy/hapi-fhir-jpaserver-example.war
to {tomcat_home}/webapps
Rename to ROOT.war (optional)
go to http://localhost:8080/
Note: It can take a minute to start up

# Build Instructions
Overwrite 
hapi-fhir\hapi-fhir-jpaserver-example\src\main\webapp\WEB-INF\web.xml
with 
hapi-fhir\hapi-fhir-jpaserver-example\src\main\webapp\WEB-INF\web-non-polling-subscription-dstu3.xml
mvn clean install

# Tests
Integration tests can be found at 
hapi-fhir\hapi-fhir-jpaserver-example\src\test\java\ca\uhn\fhir\jpa\demo\subscription


